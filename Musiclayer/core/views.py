import random
from django.shortcuts import render
from django.http import JsonResponse


# from core.serializers import MusicSerializer


note_list = {1:"C",2:"D",3:"E",4:"F",5:"G",6:"A",7:"B",8:"CH"}
length_list = {1:1,2:1/2,3:1/4,4:1/8}

def music_detail(request):
    note = note_list[random.randint(1,8)]
    length = length_list[random.randint(1,4)]
    print(note,length)
    return JsonResponse({"note":note, "length":length})
