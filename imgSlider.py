import time
import cv2
from sys import argv
import imutils
import label_image as li

xrange = range
file_name = "./note-eighth-c1-870.jpg"
model_file = "./out_mobnet/output_graph.pb"
label_file = "./out_mobnet/output_labels.txt"
input_height = 299
input_width = 299
input_mean = 0
input_std = 255
input_layer = "Placeholder"
output_layer = "final_result"

def pyramid(image, scale=1.5, minSize=(30, 30)):
	# yield the original image
	yield image
 
	# keep looping over the pyramid
	while True:
		# compute the new dimensions of the image and resize it
		w = int(image.shape[1] / scale)
		print(w)
		image = imutils.resize(image, width=w)
 
		# if the resized image does not meet the supplied minimum
		# size, then stop constructing the pyramid
		if image.shape[0] < minSize[1] or image.shape[1] < minSize[0]:
			break
 
		# yield the next image in the pyramid
		yield image

def sliding_window(image, stepSize, windowSize):
	# slide a window across the image
	for y in xrange(0, image.shape[0], windowSize[1]):
		for x in xrange(0, image.shape[1], windowSize[0]):
			# yield the current window
			yield (x, y, image[y:y + windowSize[1], x:x + windowSize[0]])


image = cv2.imread(argv[1],0)
(winW, winH) = (16,64)

#load tensorflow pretrained model

graph = li.load_graph(model_file)
t = li.read_tensor_from_image_file(
	file_name,
	input_height=input_height,
	input_width=input_width,
	input_mean=input_mean,
	input_std=input_std
	)

input_name = "import/" + input_layer
output_name = "import/" + output_layer
input_operation = graph.get_operation_by_name(input_name)
output_operation = graph.get_operation_by_name(output_name)


# # loop over the sliding window for each layer of the pyramid
# for (x, y, window) in sliding_window(image, stepSize=16, windowSize=(winW, winH)):
# 	# if the window does not meet our desired window size, ignore it
# 	if window.shape[0] != winH or window.shape[1] != winW:
# 		continue

# 	clone = image.copy()
# 	cv2.rectangle(clone, (x, y), (x + winW, y + winH), (0, 255, 0), 2)
# 	croped = clone[x:x+winH, y:y+winW]
# 	cv2.imshow("croped image", croped)

# 	#cv2.imshow("Window", clone)
# 	cv2.waitKey(1)
# 	time.sleep(0.025)

